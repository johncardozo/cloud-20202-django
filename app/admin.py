from django.contrib import admin
from .models import Pelicula, Categoria

admin.site.register(Pelicula)
admin.site.register(Categoria)
