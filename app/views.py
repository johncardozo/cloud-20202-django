from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404

from .models import Categoria, Pelicula

def index(request):
    return render(request, 'app/index.html')

def acercade(request):
    return render(request, 'app/acercade.html')

@login_required
def peliculas(request):
    # Obtiene las peliculas
    lista = Pelicula.objects.all().order_by('anio')
    # Crea el contexto
    contexto = {
        'peliculas': lista
    }
    return render(request, 'app/peliculas.html', contexto)

@login_required
def pelicula(request, id):
    # Obtiene la pelicula. Si no la encuentra genera el error 404
    obj_pelicula = get_object_or_404(Pelicula, pk=id)
    #obj_pelicula = Pelicula.objects.get(pk=id)
    # Crea el contexto
    contexto = {
        'pelicula': obj_pelicula
    }
    # Muestra el template con la información del contexto
    return render(request, 'app/pelicula.html', contexto)

@login_required
def categorias(request):
    # Obtiene todas las categorias
    lista = Categoria.objects.all()
    # Crea el contexto
    contexto = {
        'categorias': lista
    }
    # Invoca el template enviando el contexto
    return render(request, 'app/categorias.html', contexto)

@login_required
def form_crear_categoria(request):
    return render(request, 'app/formCrearCategoria.html')

@login_required
def post_crear_categoria(request):
    # Obtiene el usuario autenticado
    usuario = request.user
    # Obtiene el nombre de la categoria
    nombre = request.POST['nombre']
    # Crea el objeto
    cat = Categoria(nombre=nombre)
    # Guarda el objeto en la bd
    cat.save()
    # Redirecciona la pagina de categorias
    return redirect('app:categorias')


@login_required
def form_crear_pelicula(request):
    # Obtiene las categorias
    lista_categorias = Categoria.objects.all().order_by('nombre')
    # Crea el contexto
    contexto = {
        'categorias': lista_categorias
    }
    # Muestra el template con el contexto
    return render(request, 'app/formCrearPelicula.html', contexto)


@login_required
def post_crear_pelicula(request):
    # Obtiene los datos del formulario
    titulo = request.POST['titulo'] 
    anio = request.POST['anio'] 
    sinopsis = request.POST['sinopsis'] 
    actores = request.POST['actores'] 
    id_categoria = int(request.POST['categoria'])

    # Obtiene la categoria
    categoria = Categoria.objects.get(pk=id_categoria)

    # Crea el objeto Pelicula
    nueva_pelicula = Pelicula()
    nueva_pelicula.titulo = titulo
    nueva_pelicula.anio = anio
    nueva_pelicula.sinopsis = sinopsis
    nueva_pelicula.actores = actores
    nueva_pelicula.categoria = categoria

    # GUarda el objeto en la BD
    nueva_pelicula.save()
    
    # Redirecciona a la pagina de peliculas
    return redirect('app:peliculas')


def form_registro(request):
    return render(request, 'app/registro.html')


def post_registro(request):
    # Obtiene los datos del formulario
    nombres = request.POST['nombres'] 
    usuario = request.POST['usuario'] 
    email = request.POST['email'] 
    clave = request.POST['clave'] 

    # Crea el objeto
    u = User()
    u.username = usuario
    u.first_name = nombres
    u.email = email
    u.set_password(clave)

    # Guarda el objeto en BD
    u.save()

    return redirect('app:bienvenida')


def bienvenida(request):
    return render(request, 'app/bienvenida.html')


def form_login(request):
    return render(request, 'app/login.html')    


def post_login(request):
    # Obtiene los datos del formulario
    usuario = request.POST['usuario'] 
    clave = request.POST['clave'] 

    # Verifica si existe un usuario con las credenciales
    u = authenticate(username=usuario, password=clave)

    if u is None:
        return HttpResponse('<h1>Usuario o clave incorrectas!!!</h1><p>Revise los datos digitados</p>')   
    else:
        # Autentica el usuario en el sistema
        login(request, u)
        # Redirección al home
        return redirect('app:index')


        
def post_logout(request):
    # Cierra la sesión
    logout(request)
    # Redirección al home
    return redirect('app:index')    

    